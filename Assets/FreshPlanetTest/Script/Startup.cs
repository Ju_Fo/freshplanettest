﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Startup : MonoBehaviour
{
    public GameData GameData;
    void Start()
    {
        GameData.Init();
        UIRoot.Instance.GameData = GameData;
        UIRoot.Instance.MenuStack.Push(UIRoot.Instance.MainMenu,null);
    }
}
