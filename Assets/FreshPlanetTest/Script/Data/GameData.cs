﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData : MonoBehaviour
{
    public TextAsset jsonData;
    [HideInInspector]
    public DataRoot Data;
    public void Init()
    {
        Data = JsonUtility.FromJson<DataRoot>("{\"playListList\":" + jsonData.text + "}");
    }
}

[Serializable]
public class DataRoot
{
    public PlayList[] playListList;
}

[Serializable]
public class PlayList
{
    public string id;
    public string playlist;
    public QuestionEntry[] questions;
}

[Serializable]
public class QuestionEntry
{
    public string id;
    public int answerIndex;
    public ChoiceEntry[] choices;
    public SongEntry song;
}

[Serializable]
public class ChoiceEntry
{    
    public string artist;
    public string title;
}

[Serializable]
public class SongEntry
{
    public string id;    
    public string title;
    public string artist;
    public string picture;
    public string sample;
}