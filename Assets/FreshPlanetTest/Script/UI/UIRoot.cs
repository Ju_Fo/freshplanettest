﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIRoot : MonoBehaviour
{
    [HideInInspector]
    public GameData GameData;
    public static UIRoot Instance { get; private set; }
    public MenuStack MenuStack;
    public GameObject MainMenu;
    public GameObject QuizMenu;
    public GameObject ResultScreenMenu;
    private void Awake()
    {
        DontDestroyOnLoad(this);
        Instance = this;
    }

}
