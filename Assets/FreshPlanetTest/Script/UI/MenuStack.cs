﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuStack : MonoBehaviour
{
    public Transform MenuContainer;
    private Stack<MenuBase> _menuStack = new Stack<MenuBase>();
    private int Count => _menuStack.Count;
    public void Push(GameObject menu,object param)
    {
        var menuInstance = Instantiate(menu,MenuContainer);
        var menuComponent = menuInstance.GetComponent<MenuBase>();
        menuComponent.OnPush(param);
        _menuStack.Push(menuComponent);
    }

    public void Pop()
    {
        var menu = _menuStack.Pop();
        menu.OnPop();
        menu.transform.SetParent(null);
    }
}
