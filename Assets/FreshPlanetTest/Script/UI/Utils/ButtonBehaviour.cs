﻿﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
using System.Threading.Tasks;
namespace FreshPlanetTest.UI.Utils
{
    [DisallowMultipleComponent]
    public class ButtonBehaviour : MonoBehaviour, IPointerDownHandler, IPointerExitHandler, IPointerClickHandler, IPointerEnterHandler, IPointerUpHandler
    {
        [HideInInspector]
        public ButtonBounce ButtonBounce;
        [HideInInspector]
        private Button buttonComponent;
        private bool isEnable = true;
        private bool buttonIsDown = false;
        public Button ButtonComponent
        {
            get
            {
                if (buttonComponent == null)
                {
                    buttonComponent = GetComponent<Button>();
                }
                return buttonComponent;
            }
        }
        public virtual void Awake()
        {
            ButtonBounce = GetComponent<ButtonBounce>();
        }

        private void OnDestroy()
        {
            if (ButtonComponent != null) ButtonComponent.onClick.RemoveAllListeners();
        }

        public virtual void OnPointerDown(PointerEventData pointerEventData)
        {
            

            if (isEnable)
            {
                buttonIsDown = true;
                if (ButtonBounce != null)
                {
                    ButtonBounce.GotoState(ButtonBounce.States.Pressed);
                }
            }
        }

        public virtual void OnPointerUp(PointerEventData pointerEventData)
        {
            if (isEnable)
            {
                buttonIsDown = false;
                if (ButtonBounce != null)
                {
                    ButtonBounce.GotoState(ButtonBounce.States.Released);
                }
            }
        }

        public virtual void OnPointerEnter(PointerEventData pointerEventData)
        {
            if (isEnable && buttonIsDown)
            {
                if (ButtonBounce != null)
                {
                    ButtonBounce.GotoState(ButtonBounce.States.Pressed);
                }
            }
        }

        public virtual void OnPointerExit(PointerEventData pointerEventData)
        {
            if (isEnable && buttonIsDown)
            {
                if (ButtonBounce != null)
                {
                    ButtonBounce.GotoState(ButtonBounce.States.Released);
                }
            }
        }

        public void OnPointerClick(PointerEventData pointerEventData)
        {

        }

        virtual public bool IsEnable
        {
            get { return isEnable; }
            set
            {
                isEnable = value;
            }
        }

        public void ForceRelease()
        {
            if (ButtonBounce != null)
            {
                ButtonBounce.GotoState(ButtonBounce.States.Released);
            }
        }
    }
}