﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonBounce : MonoBehaviour
{
    public enum States
    {
        Pressed,
        Released,
        Bounce,
    }
    [SerializeField]
    public AnimationCurve _releaseCurve;
    [SerializeField]
    public AnimationCurve _pressCurve;
    [SerializeField]
    public float _releaseDuration = 0.11f;
    [SerializeField]
    public float _bounceDuration = 0.11f;
    [SerializeField]
    public float _pressDuration = 0.11f;
    [SerializeField]
    public Transform _target;
    public void GotoState(States state)
    {
        switch (state)
        {
            case States.Pressed:
                _target.DOScale(0.87f, _pressDuration).SetEase(_pressCurve).Play();
                break;
            case States.Released:
                _target.DOScale(1f, _releaseDuration).SetEase(_releaseCurve).Play();
                break;
            case States.Bounce:
                _target.localScale = new Vector3(1.2f, 1.2f, 1f);
                _target.DOScale(1f, _bounceDuration).SetEase(_releaseCurve).Play();
                break;
        }
    }
}
