﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
namespace FreshPlanetTest.UI.Utils
{
    public class ListBase : MonoBehaviour
    {
        public class ListEventData
        {
            public GameObject GameObject;
            public int ItemIndex;
            public ListEventData(GameObject gameObject, int index)
            {
                GameObject = gameObject;
                ItemIndex = index;
            }
        }

        [SerializeField]
        private RectTransform m_Content;

        [SerializeField]
        public GameObject prefab;
        public RectTransform content { get { return m_Content; } set { m_Content = value; } }

        private int _totalCount;
        public int TotalCount
        {
            get
            {
                return _totalCount;
            }
            set
            {
                _totalCount = value;
                if (_totalCount == 0)
                    ClearCells(_totalCount);
                else
                    RefillCells();
            }
        }

        //events
        [Serializable]
        public class ListEvents : UnityEvent<ListEventData> { }
        public static readonly int INVALID_INDEX = int.MinValue;
        public int MaxSelectableItems { get; set; } = 1;
        private List<int> selectedIndexes = new List<int>();
        private List<int> deselectedIndexes = new List<int>();
        public int[] SelectedIndexes
        {
            get { return selectedIndexes.ToArray(); }
            set { SetSelectedIndexes(value); }
        }
        [SerializeField]
        private int _selectedIndex;
        public int SelectedIndex
        {
            get { _selectedIndex = selectedIndexes.Count > 0 ? selectedIndexes[0] : INVALID_INDEX; return _selectedIndex; }
            set { _selectedIndex = value; SetSelectedIndexes(new int[] { _selectedIndex }); }
        }

        public bool DeselectWhenSelected { get; set; } = false;
        public bool DeselectOldest { get; set; } = true;

        //Delegates
        [SerializeField]
        private ListEvents m_OnItemData = new ListEvents();
        public ListEvents OnItemData { get { return m_OnItemData; } set { m_OnItemData = value; } }

        [SerializeField]
        private ListEvents m_OnItemTap = new ListEvents();
        public ListEvents OnItemTap { get { return m_OnItemTap; } set { m_OnItemTap = value; } }

        [SerializeField]
        private ListEvents m_OnItemTapAndHold = new ListEvents();
        public ListEvents OnItemTapAndHold { get { return m_OnItemTapAndHold; } set { m_OnItemTapAndHold = value; } }

        [SerializeField]
        private ListEvents m_OnItemSelect = new ListEvents();
        public ListEvents OnItemSelect { get { return m_OnItemSelect; } set { m_OnItemSelect = value; } }

        [SerializeField]
        private ListEvents m_OnItemDeselect = new ListEvents();

        public ListEvents OnItemDeselect { get { return m_OnItemDeselect; } set { m_OnItemDeselect = value; } }

        protected virtual bool UpdateItems(Bounds viewBounds, Bounds contentBounds) { return false; }

        private int _lastSelectedIndex = -1;

        private void ClearCells(int totalCount = -1)
        {
            if (Application.isPlaying)
            {
                if (totalCount != -1)
                {
                    var index = 0;
                    foreach (Transform child in content.transform)
                    {
                        if (index >= totalCount)
                        {
                            child.gameObject.SetActive(false);
                        }
                        index++;
                    }
                }
                selectedIndexes.Clear();
                SelectedIndex = INVALID_INDEX;
            }
        }

        private void DestroyItem(Transform transform)
        {
            var itemButton = transform.GetComponent<Button>();
            if (itemButton != null)
            {
                itemButton.onClick.RemoveAllListeners();
            }

            Destroy(transform.gameObject);
        }

        public void RefreshCells()
        {
            if (Application.isPlaying && this.isActiveAndEnabled)
            {
                // recycle items if we can
                for (int i = 0; i < content.transform.childCount; i++)
                {
                    if (i < TotalCount)
                    {
                        var item = content.transform.GetChild(i);
                        m_OnItemData?.Invoke(new ListEventData(content.transform.GetChild(i).gameObject, i));
                    }
                    else
                    {
                        DestroyItem(content.transform.GetChild(i));
                        i--;
                    }
                }
            }
        }

        public void RefillCells(int offset = 0)
        {
            if (!Application.isPlaying)
                return;

            ClearCells(_totalCount);

            if (prefab != null)
            {
                for (int i = 0; i < TotalCount; i++)
                {
                    if (i < content.childCount)
                    {
                        var currentItem = content.GetChild(i);
                        m_OnItemData?.Invoke(new ListEventData(currentItem.gameObject, i));
                        if (!currentItem.gameObject.activeSelf)
                        {
                            currentItem.gameObject.SetActive(true);
                        }
                    }
                    else
                    {
                        InstantiateNextItem(i);
                    }
                }
            }
        }
        private RectTransform InstantiateNextItem(int itemIdx)
        {
            var item = Instantiate(prefab, content);
            item.transform.SetAsLastSibling();
            item.name = "Item_" + itemIdx;
            RectTransform nextItem = item.GetComponent<RectTransform>();
            var itemButton = item.GetComponent<Button>();
            if (itemButton != null)
            {
                itemButton.onClick.RemoveAllListeners();
                itemButton.onClick.AddListener(() => onItemClickHandler(item, itemIdx));
            }

            m_OnItemData?.Invoke(new ListEventData(nextItem.gameObject, itemIdx));
            nextItem.gameObject.SetActive(true);
            return nextItem;
        }

        public List<GameObject> GetAllItem()
        {
            List<GameObject> allItem = new List<GameObject>();
            for (int i = 0; i < content.transform.childCount; i++)
            {
                GameObject item = content.transform.GetChild(i).gameObject;
                allItem.Add(item);
            }
            return allItem;
        }
        private void onItemClickHandler(GameObject item = null, int itemIdx = 0)
        {
            m_OnItemTap?.Invoke(new ListEventData(item, itemIdx));
            AddSelectedIndex(itemIdx);
        }


        private void onItemTapAndHoldHandler(GameObject item = null, int itemIdx = 0)
        {
            m_OnItemTapAndHold?.Invoke(new ListEventData(item, itemIdx));
        }

        protected void SetSelectedIndexes(int[] indexes)
        {
            while (selectedIndexes.Count > 0)
            {
                int selectedIndex = selectedIndexes[0];
                selectedIndexes.RemoveAt(0);
                DeselectItemAt(selectedIndex);
            }

            for (int i = 0; i < indexes.Length; ++i)
            {
                if (i < MaxSelectableItems && indexes[i] >= 0)
                {
                    int newIndex = indexes[i];
                    selectedIndexes.Add(newIndex);
                    SelectItemAt(newIndex);
                }
            }
        }
        protected void DeselectItemAt(int index)
        {
            var item = GetItemByDataIndex(index);
            ListEventData e = new ListEventData(item?.gameObject, index);
            m_OnItemDeselect?.Invoke(e);
        }

        protected void SelectItemAt(int index)
        {
            var item = GetItemByDataIndex(index);
            ListEventData e = new ListEventData(item?.gameObject, index);
            _lastSelectedIndex = index;
            OnItemSelect?.Invoke(e);
        }
        virtual public Transform GetItemByDataIndex(int index)
        {
            if (IsIndexOutOfRange(index)) return null;
            else return m_Content.transform.GetChild(index);
        }

        protected bool IsIndexOutOfRange(int index)
        {
            return index < 0 || index >= TotalCount;
        }

        protected void AddSelectedIndex(int index)
        {
            if (MaxSelectableItems <= 0)
            {
                return;
            }

            if (selectedIndexes.Contains(index) && DeselectWhenSelected)
            {
                selectedIndexes.Remove(index);
                DeselectItemAt(index);
            }
            else
            {
                if (!selectedIndexes.Contains(index))
                {
                    if (selectedIndexes.Count < MaxSelectableItems)
                    {
                        selectedIndexes.Add(index);
                        SelectItemAt(index);
                    }
                    else if (DeselectOldest)
                    {
                        int previousSelectedIndex = selectedIndexes[0];
                        selectedIndexes.Remove(previousSelectedIndex);
                        DeselectItemAt(previousSelectedIndex);
                        selectedIndexes.Add(index);
                        SelectItemAt(index);
                    }
                }
            }
        }


    }
}
