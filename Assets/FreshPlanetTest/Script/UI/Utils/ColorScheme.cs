﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorScheme : MonoBehaviour
{
    [SerializeField]
    public List<Color> Colors;
}
