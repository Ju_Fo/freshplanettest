﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ColorSchemeManager : MonoBehaviour
{
    private List<ColorScheme> Items;

    void Awake()
    {
        Items = GetComponentsInChildren<ColorScheme>().ToList();
    }

    public void ApplyColorScheme(int index)
    {
        if (Items != null)
        {
            for (int i = 0; i < Items.Count; i++)
            {
                var item = Items[i].gameObject.GetComponent<MaskableGraphic>();
                var colorScheme = Items[i];
                item.color = colorScheme.Colors[index];
            }
        }
    }
}
