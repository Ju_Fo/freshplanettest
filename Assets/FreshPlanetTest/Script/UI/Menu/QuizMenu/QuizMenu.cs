﻿using FreshPlanetTest.UI.Utils;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using static FreshPlanetTest.UI.Utils.ListBase;

public class QuizMenu : MenuBase
{
    public TextMeshProUGUI PlayListName;
    public ListBase AnswerList;
    private PlayList _data;
    private int _currentQuestionIndex = 0;
    private List<ChoiceEntry> _currentChoices;
    private AudioSource _audioSource;
    private bool _isArtist = false;
    private List<bool> _answerResult = new List<bool>();
    private Coroutine _audioClipCoroutine;
    public override void OnPush(object param)
    {
        base.OnPush(param);
        _data = param as PlayList;
        PlayListName.text = _data.playlist;
        AnswerList.OnItemData.AddListener(OnItemData);
        AnswerList.OnItemTap.AddListener(OnItemTap);
        _audioSource = GetComponent<AudioSource>();
        SetupQuestion();
    }

    public override void OnPop()
    {
        base.OnPop();
        if (_audioClipCoroutine != null)
        {
            StopCoroutine(_audioClipCoroutine);
        }
        KillAudioClip();
    }

    private void SetupQuestion()
    {
        if(_currentQuestionIndex >= _data.questions.Count())
        {
            var param = new ResultScreenMenuParams();
            param.AnswerResult = _answerResult;
            param.PlayList = _data;
            UIRoot.Instance.MenuStack.Pop();
            UIRoot.Instance.MenuStack.Push(UIRoot.Instance.ResultScreenMenu, param);
        }
        else
        {
            var rn = new System.Random();
            _isArtist = rn.Next(0, 4)%2==0;
            _currentChoices = _data.questions[_currentQuestionIndex].choices.ToList();
            AnswerList.TotalCount = _currentChoices.Count;
            if(_audioClipCoroutine != null)
            {
                StopCoroutine(_audioClipCoroutine);
            }
            _audioClipCoroutine = StartCoroutine(PlayAudioClip());
        }
    }

    private void OnItemData(ListEventData listEvent)
    {
        var item = listEvent.GameObject.GetComponent<AnswerButton>();
        listEvent.GameObject.GetComponent<Button>().enabled = true;
        item.ResetColor();
        item.SetData(_data.questions[_currentQuestionIndex].choices[listEvent.ItemIndex], _isArtist);
    }

    private void OnItemTap(ListEventData listEvent)
    {
        KillAudioClip();
        var buttonList = AnswerList.GetAllItem();
        for (int i = 0; i < buttonList.Count; i++)
        {
            buttonList[i].GetComponent<Button>().enabled = false;
        }
        var good = listEvent.ItemIndex == _data.questions[_currentQuestionIndex].answerIndex;
        AnswerButton GoodButton;
        AnswerButton BadButton;
        _answerResult.Add(good);
        if (good)
        {
            GoodButton = buttonList[listEvent.ItemIndex].GetComponent<AnswerButton>();
        }
        else
        {
            BadButton = buttonList[listEvent.ItemIndex].GetComponent<AnswerButton>();
            BadButton.ShowBad();
            GoodButton = buttonList[_data.questions[_currentQuestionIndex].answerIndex].GetComponent<AnswerButton>();            
        }
        GoodButton.ShowGood();
        
        StartCoroutine(AnswerValidated());
    }

    private IEnumerator AnswerValidated()
    {
        yield return new WaitForSeconds(2f);
        _currentQuestionIndex++;
        SetupQuestion();
    }

    private IEnumerator PlayAudioClip()
    {
        using (UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(_data.questions[_currentQuestionIndex].song.sample, AudioType.WAV))
        {
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError)
            {
                Debug.Log(www.error);
            }
            else
            {
                AudioClip myClip = DownloadHandlerAudioClip.GetContent(www);
                KillAudioClip();
                _audioSource.clip = myClip;
                _audioSource.Play();
            }
        }
    }

    private void KillAudioClip()
    {
        if (_audioSource.clip != null)
        {
            _audioSource.Stop();
        }
    }

}
