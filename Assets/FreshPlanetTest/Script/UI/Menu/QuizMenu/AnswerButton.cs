﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AnswerButton : MonoBehaviour
{
    public TextMeshProUGUI ButtonLabel;
    public ColorSchemeManager ColorManager;

    public void SetData(ChoiceEntry data, bool isArist)
    {
        if(isArist)
        {
            ButtonLabel.text = data.artist;
        }
        else
        { 
            ButtonLabel.text = data.title;
        }
    }

    public void ShowGood()
    {
        ColorManager.ApplyColorScheme(2);
    }

    public void ShowBad()
    {
        ColorManager.ApplyColorScheme(1);
    }

    public void ResetColor()
    {
        ColorManager.ApplyColorScheme(0);
    }
}
