﻿using FreshPlanetTest.UI.Utils;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using static FreshPlanetTest.UI.Utils.ListBase;

public class ResultScreenMenu : MenuBase
{
    public TextMeshProUGUI PlayListName;
    public TextMeshProUGUI Score;
    public ListBase AnswerList;
    private ResultScreenMenuParams _data;
    private List<QuestionEntry> _questionList;
    public override void OnPush(object param)
    {
        base.OnPush(param);
        _data = param as ResultScreenMenuParams;
        PlayListName.text = _data.PlayList.playlist;
        _questionList = _data.PlayList.questions.ToList();
        AnswerList.OnItemData.AddListener(OnItemData);
        AnswerList.TotalCount = _questionList.Count;
        var scoreCount = 0;
        for (int i = 0; i < _data.AnswerResult.Count; i++)
        {
            if (_data.AnswerResult[i])
            {
                scoreCount++;
            }
        }
        Score.text = scoreCount.ToString()+" / "+ _data.AnswerResult.Count.ToString();
    }

    private void OnItemData(ListEventData listEvent)
    {
        var item = listEvent.GameObject.GetComponent<AnswerEntry>();
        item.SetData(_questionList[listEvent.ItemIndex].song, _data.AnswerResult[listEvent.ItemIndex]);
    }

    public void OnContinue()
    {
        UIRoot.Instance.MenuStack.Pop();
    }
}

public class ResultScreenMenuParams
{
    public List<bool> AnswerResult;
    public PlayList PlayList;
}

