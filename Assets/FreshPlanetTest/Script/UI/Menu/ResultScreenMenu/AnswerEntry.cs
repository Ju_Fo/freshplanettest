﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AnswerEntry : MonoBehaviour
{
    public TextMeshProUGUI SongName;
    public TextMeshProUGUI ArtistName;
    public Image AlbumCover;
    public GameObject WrongIco;
    public GameObject RightIco;
    private SongEntry _data;

    public void SetData(SongEntry data, bool wasRight)
    {
        _data = data;
        SongName.text = data.title;
        ArtistName.text = data.artist;
        WrongIco.SetActive(!wasRight);
        RightIco.SetActive(wasRight);
        StartCoroutine(FetchImage());
    }
    private IEnumerator FetchImage()
    {
        using (WWW www = new WWW(_data.picture))
        {
            yield return www;
            Renderer renderer = GetComponent<Renderer>();
            AlbumCover.sprite = Sprite.Create(www.texture, new Rect(0, 0, 100, 100), new Vector2()); ;
        }
    }
}
