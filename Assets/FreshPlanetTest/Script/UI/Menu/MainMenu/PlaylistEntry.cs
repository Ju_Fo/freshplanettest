﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlaylistEntry : MonoBehaviour
{
    public TextMeshProUGUI ButtonLabel;
    public PlayList Data;
    public void SetData(PlayList data)
    {
        Data = data;
        ButtonLabel.text = data.playlist;
    }

}
