﻿using FreshPlanetTest.UI.Utils;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using static FreshPlanetTest.UI.Utils.ListBase;

public class MainMenu : MenuBase
{
    public ListBase PlayListView;
    private List<PlayList> _playListData;
    public override void OnPush(object param)
    {
        base.OnPush(param);        

        _playListData = UIRoot.Instance.GameData.Data.playListList.ToList();

        PlayListView.OnItemData.AddListener(OnItemData);
        PlayListView.OnItemTap.AddListener(OnItemTap);
        PlayListView.TotalCount = _playListData.Count;
    }

    private void OnItemData(ListEventData listEvent)
    {
        var playlistEntry = listEvent.GameObject.GetComponent<PlaylistEntry>();
        playlistEntry.SetData(_playListData[listEvent.ItemIndex]);
    }

    private void OnItemTap(ListEventData listEvent)
    {
        var playlistEntry = listEvent.GameObject.GetComponent<PlaylistEntry>();
        Debug.Log(playlistEntry.Data.playlist);
        UIRoot.Instance.MenuStack.Push(UIRoot.Instance.QuizMenu, playlistEntry.Data);
    }
}
