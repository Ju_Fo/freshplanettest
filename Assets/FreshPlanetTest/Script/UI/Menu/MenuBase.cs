﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuBase : MonoBehaviour
{
   virtual public void OnPush(object param)
   {

   }

   virtual public void OnPop()
   {

   }
}
